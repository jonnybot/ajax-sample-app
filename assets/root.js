"use strict";

(function () {
    window.addEventListener('load', loadHandler);

    function renderResponseToUser(dataString) {
        getLoadingGif().setAttribute('hidden', '');
        let body = document.querySelector('body');
        body.innerHTML += `<p>${dataString}</p>`;
    }
    
    let postFlight = (response, genus, species) => {
        if (response.ok) {
            console.log("Request OK");
            fetch(`http://localhost:3001/api/${genus}/${species}`, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then((putResponse) => {
                if (putResponse.ok) {
                    return putResponse.json();
                } else if (putResponse.status === 404) {
                    console.log("Request Not found");
                    renderResponseToUser(`Creature not found`);
                } else if (putResponse.status >= 500) {
                    console.log("Server error");
                    renderResponseToUser('Hmmm... something seems to be wrong with the creature database. Try again later.');
                }
            }).then((json) => {
                console.log(json);
                renderResponseToUser(`${json.genus} ${json.species} has ${json.population} members`);
            });
        } else {
            console.error("Preflight failed");
        }
    };

    function loadHandler() {
        let button = document.querySelector('button#send');
        button.addEventListener('click', () => {
            getLoadingGif().removeAttribute('hidden');
            let inputs = document.querySelectorAll('input');
            let genus = inputs[0].value;
            let species = inputs[1].value;
            fetch(`http://localhost:3001/api/${genus}/${species}`, {
                method: 'OPTIONS',
                headers: {
                    'Access-Control-Request-Method': 'PUT',
                    'Access-Control-Request-Headers': 'Content-Type'
                    //'Content-Type': 'application/json'
                }
            }).then((response) => postFlight(response, genus, species));

            /*
            fetch("//google.com", {
                mode: 'no-cors' /*We can pass a configuration object to fetch to allow cross-domain requests. Notably, this will remove the automatically-added CORS headers, which the serving API may not like. Only use the no-cors option when you're deliberately fetching "opaque" 
            }).then( (response) => {
                console.log("Got response from Google");
                if (response.ok) {
                    console.log(response.type); //While we can read the request type...
                    console.log(response.responseText); //we still can't read the body due to same origin
                }
            });*/

        });
    }
    
    function getLoadingGif() {
        return document.getElementById('loader');
    }
})();
