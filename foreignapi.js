"use strict";

const express = require('express');
const hoffman = require('hoffman');
const path = require('path');
const bodyParser = require('body-parser');

const app = express(); //express module returns a function, not an object!
app.set('views', path.join(__dirname, 'views')); // path to your templates
app.set('view engine', 'dust');
app.engine('dust', hoffman.__express());

app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.json());

app.get("/api/thing", (request, response) => {
    response.setHeader('Content-Type', 'application/json');
    let origin = request.get('Origin');
    //if ( === 'http://localhost:3000') {
    response.setHeader('Access-Control-Allow-Origin', origin);
    response.send(JSON.stringify({
        data: 'abcdefghijklmnopqrstuvwxyz'
    }));
    /*} else {
        response.statusCode = 403;
        response.send("Buzz off");
    }*/
});

app.options("/api/:genus/:species", (request, response) => {
    response.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');
    response.setHeader('Access-Control-Allow-Methods', 'PUT, GET, OPTIONS');
    response.setHeader('Access-Control-Allow-Headers', 'Content-Type');
    response.send('');
});

app.put("/api/:genus/:species", (request, response) => {
    setTimeout(() => {
        if (request.get('Origin') === 'http://localhost:3000') {
            response.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');
            response.setHeader('Content-Type', 'application/json');
            let genus = request.params.genus;
            let species = request.params.species;
            response.send(JSON.stringify({
                genus: genus,
                species: species,
                population: (genus.length + species.length)
            }));
        } else {
            response.statusCode = 403;
            response.send("Buzz off");
        }
        
    }, 2000);
});


app.listen(3001, () => {
    console.log("My app is listening on port 3001!");
});
